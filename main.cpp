#include "cefconfiguration.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CefConfiguration w;
    w.show();

    return a.exec();
}

// Joel Salminen - joel.salminen@gmail.com
