#include "configuration.h"

Configuration::Configuration()
{

}

Configuration::Configuration(
    std::vector<Input *> inputs,
    std::vector<View *> views,
    std::vector<Output *> outputs
){
    this->inputs = inputs;
    this->outputs = outputs;
    this->views = views;
}


std::vector<Input *> Configuration::getInputs(){
    return this->inputs;
}

std::vector<View *> Configuration::getViews(){
    return this->views;
}

std::vector<Output *> Configuration::getOutputs(){
    return this->outputs;
}

void Configuration::addView(View *v){
    this->views.push_back(v);
}


