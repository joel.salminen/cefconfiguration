#include "cefconfiguration.h"
#include "ui_cefconfiguration.h"
#include <QInputDialog>
#include <QMessageBox>


CefConfiguration::CefConfiguration(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CefConfiguration)
{
    ui->setupUi(this);


    this->conf = initializeConfiguration();

    foreach(Input * input, conf.getInputs()){
        QPushButton *button = new QPushButton();
        button->setText(QString::fromStdString(input->getName()));
        ui->inputButtonLayout->addWidget(button);
        connect(button, SIGNAL(clicked()), this, SLOT(setSelectedInput()));
    }

    foreach(Output *output, conf.getOutputs()){
        QPushButton *button = new QPushButton();
        button->setText(QString::fromStdString(output->getName()));
        ui->outputButtonLayout->addWidget(button);
        connect(button, SIGNAL(clicked()), this, SLOT(setSelectedOutput()));
    }

    ui->multiViewTypeSelector->addItem("Single view");
    ui->multiViewTypeSelector->addItem("Multi view");
    QPixmap pix(":/resources/multiview.png");
    ui->multiViewImage->setPixmap(pix);


}

void CefConfiguration::setSelectedInput(){
    QString senderButtonText = qobject_cast<QPushButton*>(sender())->text();
    foreach(Input *input, conf.getInputs()){
        if (QString::fromStdString(input->getName()) == senderButtonText){
            selectedInput = input;
            break;
        }
    }
    ui->stackedWidget->setCurrentIndex(1);
    ui->inputName->setText(senderButtonText);
}


void CefConfiguration::setSelectedOutput(){
    QString senderButtonText = qobject_cast<QPushButton*>(sender())->text();
    Output currentOutput;

    foreach(Output * output, conf.getOutputs()){
        if (QString::fromStdString(output->getName()) == senderButtonText){
            selectedOutput = output;
            break;
        }
    }
    ui->stackedWidget->setCurrentIndex(3);
    ui->outputName->setText(senderButtonText);

    foreach(View *view, selectedOutput->getViews()){
        QPushButton *inputButton = new QPushButton();
        inputButton->setText(QString::fromStdString(view->getName()));
        ui->outputViews->addWidget(inputButton);
        connect(inputButton, SIGNAL(clicked()), this, SLOT(setSelectedView()));
    }


}


void CefConfiguration::setSelectedView(){
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    QString senderButtonText = button->text();

    foreach(View * view, conf.getViews()){
        if (QString::fromStdString(view->getName()) == senderButtonText){
            selectedView = view;
            break;
        }
    }


    foreach(View *view, this->selectedOutput->getViews()){
        if (QString::fromStdString(view->getName()) == senderButtonText){

            // clear selectInpuButtons
            while(QLayoutItem * item = ui->selectInputButtons->takeAt(0)){
                delete item->widget();
                delete item;
            }

            while(QLayoutItem * item = ui->selectInputLabels->takeAt(0)){
                delete item->widget();
                delete item;
            }

            ui->multiViewImage->show();
            if(view->getInputType() == "Single view"){
                ui->multiViewImage->hide();
            }

            // set input type
            std::string inputType = view->getInputType();
            ui->multiViewTypeSelector->setCurrentText(QString::fromStdString(inputType));


            int labelIndex = 1;
            foreach(Input *input, view->getInputs()){
                QComboBox *combobox = new QComboBox();
                foreach(Input *input2, conf.getInputs()){
                    combobox->addItem(QString::fromStdString(input2->getName()));
                }

                combobox->setCurrentText(QString::fromStdString(input->getName()));
                ui->selectInputButtons->addWidget(combobox);

                QLabel *label = new QLabel();
                label->setText(QString::fromStdString("Select input" + std::to_string(labelIndex)));
                label->alignment();
                labelIndex++;
                ui->selectInputLabels->addWidget(label);
            }
            break;
        }
    }
    ui->stackedWidget->setCurrentIndex(4);
    ui->buttonViewName->setText(senderButtonText);
}


CefConfiguration::~CefConfiguration()
{
    delete ui;
}


void CefConfiguration::goToMainMenu(){
    ui->stackedWidget->setCurrentIndex(0);
}


void CefConfiguration::on_buttonBack_clicked()
{
    goToMainMenu();
}


void CefConfiguration::on_buttonBack_2_clicked()
{
    while(QLayoutItem * item = ui->outputViews->takeAt(0)){
        if(item->layout()){
            delete item->layout();
        }
        if(item->widget()){
            delete item->widget();
        }
        delete item;

    }
    goToMainMenu();

}

void CefConfiguration::on_buttonCancelView_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);

}


Configuration CefConfiguration::initializeConfiguration(){

    std::vector<Input *> inputs = initializeInputs();
    std::vector<View *> views = initializeViews(inputs);
    std::vector<Output *> outputs= initializeOutputs(views);

    Configuration * configuration = new Configuration(inputs, views, outputs);
    return *configuration;
}

std::vector<Input *> CefConfiguration::initializeInputs(){
    std::vector <Input *> inputs;
    for (int  i = 1; i<9; ++i){
        Input * input = new Input("Input" + std::to_string(i));
        inputs.push_back(input);
    }
    return inputs;
}

std::vector<View *> CefConfiguration::initializeViews(std::vector<Input *> inputs){
    std::vector<View *> views;


    std::vector<Input *> newInputs;
    newInputs.push_back(inputs.at(0));
    std::string inputType = "Single view";
    View * view = new View("view1", newInputs, inputType);
    views.push_back(view);

    std::vector<Input *> newInputs2;
    newInputs2.push_back(inputs.at(1));
    view = new View("view2", newInputs2, inputType);
    views.push_back(view);

    std::vector<Input *> newInputs3;
    newInputs3.push_back(inputs.at(2));
    view = new View("view3", newInputs3, inputType);
    views.push_back(view);

    std::vector<Input *> newInputs4;
    newInputs4.push_back(inputs.at(3));
    view = new View("view4", newInputs4, inputType);
    views.push_back(view);

    std::vector<Input *> inputs2;
    for (unsigned i = 0; i<4; ++i){
        inputs2.push_back(inputs.at(i));
    }
    inputType = "Multi view";
    view = new View("multi1", inputs2, inputType);
    views.push_back(view);
    return views;
}

std::vector<Output *> CefConfiguration::initializeOutputs(std::vector<View *> views){
    std::vector<Output *> outputs;
    std::string outputName = "Output0";
    Output * output = new Output(outputName, views);
    outputs.push_back(output);

    for (unsigned i = 1; i<8; ++i){
        std::string outputName = "Output" + std::to_string(i);
        std::vector <View *> emptyView;
        Output * output = new Output(outputName, emptyView);
        outputs.push_back(output);
    }
    return outputs;
}

void CefConfiguration::on_buttonViewName_clicked(){
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    QString senderButtonText = button->text();
    std::string newName = QInputDialog::getText(this, "Rename view", "Name").toStdString();

    if(newName.length() == 0){
        return;
    }
    QString oldName = "";
    for(View *view : conf.getViews()){
        oldName = QString::fromStdString(view->getName());
        if(oldName == senderButtonText){
            view->setName(newName);
            button->setText(QString::fromStdString(newName));
            break;
        }
    }
    updateViewButtons(oldName, QString::fromStdString(newName));



}

void CefConfiguration::updateViewButtons(QString prevButtonText, QString newButtonText){
    int count = ui->outputViews->count();
    for(int i=0; i < count; i++){
        QWidget *widget = ui->outputViews->itemAt(i)->widget();
        QPushButton *button = dynamic_cast<QPushButton*>(widget);
        if(button->text() == prevButtonText){
            button->setText(newButtonText);
            break;
        }
    }
}



void CefConfiguration::on_buttonSaveView_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
}


void CefConfiguration::on_setOutputNameButton_clicked()
{
    std::string newName = QInputDialog::getText(this, "Rename view", "Name").toStdString();
    if(newName.length() == 0){
        return;
    }
    QString oldName = "";
    for(Output * output : conf.getOutputs()){
        oldName = QString::fromStdString(output->getName());
        if(oldName == ui->outputName->text()){
            output->setName(newName);
            ui->outputName->setText(QString::fromStdString(newName));
            break;
        }
    }
    updateOutputButtons(oldName, QString::fromStdString(newName));
}

void CefConfiguration::on_buttonSetInputName_clicked()
{
    std::string newName = QInputDialog::getText(this, "Rename view", "Name").toStdString();
    if(newName.length() == 0){
        return;
    }

    QString oldName = "";
    for(Input * input: conf.getInputs()){
        oldName = QString::fromStdString(selectedInput->getName());
        if(QString::fromStdString(input->getName()) == oldName){
            input->setName(newName);
            ui->inputName->setText(QString::fromStdString(newName));
            break;
        }
    }
    updateInputButtons(oldName, QString::fromStdString(newName));
}

void CefConfiguration::updateInputButtons(QString prevButtonText, QString newButtonText){
    int count = ui->inputButtonLayout->count();
    for(int i=0; i < count; i++){
        QWidget *widget = ui->inputButtonLayout->itemAt(i)->widget();
        QPushButton *button = dynamic_cast<QPushButton*>(widget);
        if(button->text() == prevButtonText){
            button->setText(newButtonText);
            break;
        }
    }
}

void CefConfiguration::updateOutputButtons(QString prevButtonText, QString newButtonText){
    int count = ui->outputButtonLayout->count();
    for(int i=0; i < count; i++){
        QWidget *widget = ui->outputButtonLayout->itemAt(i)->widget();
        QPushButton *button = dynamic_cast<QPushButton*>(widget);
        if(button->text() == prevButtonText){
            button->setText(newButtonText);
            break;
        }
    }
}

void CefConfiguration::on_multiViewTypeSelector_activated(const QString &arg1)
{
    std::string currentInputType = selectedView->getInputType();
    if (QString::fromStdString(currentInputType) == arg1){
        return;
    }


    if(arg1 == "Multi view"){
        int labelIndex = 2;
        selectedView->setInputType(arg1.toStdString());
        for (int i = 0; i<3; i++){
            QComboBox *combobox = new QComboBox();
            foreach(Input *input2, conf.getInputs()){
                combobox->addItem(QString::fromStdString(input2->getName()));
            }

            Input *input = conf.getInputs().at(0);
            selectedView->addInput(input);


            combobox->setCurrentText(QString::fromStdString(conf.getInputs().at(0)->getName()));
            ui->selectInputButtons->addWidget(combobox);
            ui->multiViewImage->show();
            selectedView->setInputType("Single View");


            QLabel *label = new QLabel();
            label->setText(QString::fromStdString("Select input" + std::to_string(labelIndex)));
            label->alignment();
            labelIndex++;
            ui->selectInputLabels->addWidget(label);
        }
        return;
    }
    if(arg1 == "Single view"){

        Input *input = selectedView->getInputs().at(0);

        std::vector <Input *> newInputs;
        newInputs.push_back(input);
        selectedView->setInputs(newInputs);

        while(QLayoutItem * item = ui->selectInputButtons->takeAt(0)){
            if(item->layout()){
                delete item->layout();
            }
            if(item->widget()){
                delete item->widget();
            }
            delete item;
        }

        while(QLayoutItem * item = ui->selectInputLabels->takeAt(0)){
            if(item->layout()){
                delete item->layout();
            }
            if(item->widget()){
                delete item->widget();
            }
            delete item;
        }


        int labelIndex = 1;
        foreach(Input *input, selectedView->getInputs()){
            QComboBox *combobox = new QComboBox();
            foreach(Input *input2, conf.getInputs()){
                combobox->addItem(QString::fromStdString(input2->getName()));
            }

            combobox->setCurrentText(QString::fromStdString(input->getName()));
            ui->selectInputButtons->addWidget(combobox);

            QLabel *label = new QLabel();
            label->setText(QString::fromStdString("Select input" + std::to_string(labelIndex)));
            label->alignment();
            labelIndex++;
            ui->selectInputLabels->addWidget(label);
        }
        selectedView->setInputType("Single View");
        ui->multiViewImage->hide();



    }

    return;
}

// Add View
void CefConfiguration::on_pushButton_2_clicked()
{
    if(selectedOutput->getViews().size() < 5){
        std::string name = "new view";
        std::string inputType = "Single View";
        std::vector <Input *> i;
        View *view = new View(name, i, inputType);
        selectedOutput->addView(view);

        while(QLayoutItem * item = ui->outputViews->takeAt(0)){
            if(item->layout()){
                delete item->layout();
            }
            if(item->widget()){
                delete item->widget();
            }
            delete item;

        }

        foreach(View *v, selectedOutput->getViews()){
            QPushButton *button = new QPushButton();
            button->setText(QString::fromStdString(v->getName()));
            ui->outputViews->addWidget(button);
            connect(button, SIGNAL(clicked()), this, SLOT(setSelectedView()));
        }

    }

}

void CefConfiguration::on_pushButton_3_clicked()
{
    QMessageBox *b = new QMessageBox();
    b->setText("Firmware updated.");
    b->exec();
}

void CefConfiguration::on_buttonDeleteView_clicked()
{
    std::vector <View *> newViews;
    foreach(View *view, selectedOutput->getViews()){
        if(view->getName() != selectedView->getName()){
            newViews.push_back(view);
        }
    }
    selectedOutput->setViews(newViews);
    ui->stackedWidget->setCurrentIndex(3);


    while(QLayoutItem * item = ui->outputViews->takeAt(0)){
        if(item->layout()){
            delete item->layout();
        }
        if(item->widget()){
            delete item->widget();
        }
        delete item;

    }

    foreach(View *view, selectedOutput->getViews()){
        QPushButton *inputButton = new QPushButton();
        inputButton->setText(QString::fromStdString(view->getName()));
        ui->outputViews->addWidget(inputButton);
        connect(inputButton, SIGNAL(clicked()), this, SLOT(setSelectedView()));
    }

}
