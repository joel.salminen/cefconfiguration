#ifndef VIEW_H
#define VIEW_H

#include <vector>
#include "input.h"

class View
{
public:
    View();
    View(std::string name, std::vector<Input *> inputs, std::string inputType);

    std::string getName();
    void setName(std::string name);

    int getPosition();
    void setPosition(int position);

    std::vector<Input *> getInputs();
    void setInputs(std::vector<Input *> inputs);

    std::string getInputType();
    void setInputType(std::string inputType);
    void addInput(Input *i);

private:
    std::vector<Input *> inputs;
    std::string name;
    int position;
    std::string inputType;

};

#endif // VIEW_H

// Joel Salminen - joel.salminen@gmail.com
