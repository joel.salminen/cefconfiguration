#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <iostream>
#include "output.h"

class Configuration
{
public:
    Configuration();
    Configuration(std::vector<Input *> inputs, std::vector<View *> views, std::vector<Output *> outputs);
    void setName(std::string newName);
    std::string getName();
    void addView(View *v);


    std::vector<Input *> getInputs();
    std::vector<View *> getViews();
    std::vector<Output *> getOutputs();


private:
    std::string name;
    std::vector<Input *> inputs;
    std::vector<View *> views;
    std::vector<Output *> outputs;

};

#endif // CONFIGURATION_H


