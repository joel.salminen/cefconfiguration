#ifndef OUTPUT_H
#define OUTPUT_H

#include "view.h"
#include <vector>

class Output
{
public:
    Output();
    Output(std::string name, std::vector<View *> views);
    void setName(std::string newName);
    std::string getName();
    std::vector <View *> getViews();
    void addView(View *v);
    void setViews(std::vector <View *> views);

private:
    std::string name;
    std::vector<View *> views;
};

#endif // OUTPUT_H

// Joel Salminen - joel.salminen@gmail.com
