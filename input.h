#ifndef INPUT_H
#define INPUT_H

#include <iostream>

class Input
{
public:
    Input();
    Input(std::string name);
    std::string getName();
    void setName(std::string newName);

private:
    std::string name;
};



#endif // INPUT_H

// Joel Salminen - joel.salminen@gmail.com
