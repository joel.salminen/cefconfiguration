#include "view.h"

View::View()
{

}

View::View(std::string name, std::vector<Input *> inputs, std::string inputType){
    this->name = name;
    this->inputs = inputs;
    this->inputType = inputType;
}

std::string View::getName(){
    return this->name;
}

void View::setName(std::string name){
    this->name = name;
}

std::vector<Input *> View::getInputs(){
    return this->inputs;
}

void View::setInputs(std::vector<Input *> inputs){
    this->inputs = inputs;
}

std::string View::getInputType(){
    return this->inputType;
}

void View::setInputType(std::string inputType){
    this->inputType = inputType;
}

void View::addInput(Input *i){
    this->inputs.push_back(i);
}

// Joel Salminen - joel.salminen@gmail.com
