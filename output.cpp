#include "output.h"

Output::Output()
{

}

Output::Output(std::string name, std::vector<View *> views){
    this->name = name;
    this->views = views;

}

std::string Output::getName(){
    return (this->name);
}

void Output::setName(std::string name){
    this->name = name;
}


std::vector <View *> Output::getViews(){
    return (this->views);
}

void Output::addView(View *v){
    this->views.push_back(v);
}

void Output::setViews(std::vector <View *> views){
    this->views = views;
}

// Joel Salminen - joel.salminen@gmail.com
