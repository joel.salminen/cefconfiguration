#ifndef CEFCONFIGURATION_H
#define CEFCONFIGURATION_H

#include <QMainWindow>
#include <QDebug>
#include <QPushButton>
#include <QComboBox>
#include "input.h"
#include "view.h"
#include "output.h"
#include "configuration.h"

namespace Ui {
class CefConfiguration;
}

class CefConfiguration : public QMainWindow
{
    Q_OBJECT

public:
    explicit CefConfiguration(QWidget *parent = nullptr);
    ~CefConfiguration();

private:
    Ui::CefConfiguration *ui;
    std::vector <Input *> initializeInputs();
    std::vector <View *> initializeViews(std::vector<Input *> inputs);
    std::vector <Output *> initializeOutputs(std::vector <View *> views);
    Configuration initializeConfiguration();
    Configuration conf;

    Input *selectedInput;
    Output *selectedOutput;
    View *selectedView;
    void goToMainMenu();

private slots:
    void setSelectedInput();
    void setSelectedOutput();
    void setSelectedView();
    void on_buttonBack_clicked();
    void on_buttonBack_2_clicked();
    void on_buttonCancelView_clicked();
    void on_buttonViewName_clicked();
    void on_buttonSaveView_clicked();
    void on_setOutputNameButton_clicked();
    void on_buttonSetInputName_clicked();
    void updateInputButtons(QString prevButtonText, QString newButtonText);
    void updateOutputButtons(QString prevButtonText, QString newButtonText);
    void updateViewButtons(QString prevButtonText, QString newButtonText);
    void on_multiViewTypeSelector_activated(const QString &arg1);
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_buttonDeleteView_clicked();
};



#endif // CEFCONFIGURATION_H

// Joel Salminen - joel.salminen@gmail.com
